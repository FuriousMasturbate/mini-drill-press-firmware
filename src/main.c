/**
 * scheduler.c
 * a simple tunable co-operative
 * task scheduler. capable of
 * running all the tasks in one cycle.
 */

#include "iomacros.h"
#include "registers.h"
#include "sch.h"
#include "tasks.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdint.h>

#define F_CPU 8000000UL

int
main (void)
{
	registers_init ();
	sei ();

	sch_task_create (debounce, 1, PENDING); // 0 4
	sch_task_create (button_event_handler, 13, PENDING); // 1 50

	sch_task_create (motor_startup, 9, SUSPENDED); // 2 35
	sch_task_create (motor_shutdown, 17, SUSPENDED); // 3 67
	sch_task_create (motor_rampup, 17, SUSPENDED); // 4 67
	sch_task_create (motor_rampdown, 17, SUSPENDED); // 5 67

	sch_task_create (blink, 27, PENDING); // 6 100
	sch_task_create (blink_upper, 85, SUSPENDED); // 7 340
	sch_task_create (blink_upper_secondary, 1, SUSPENDED); // 8 4

	sch_task_create (blink_lower, 14, SUSPENDED); // 9 55
	sch_task_create (blink_lower_secondary, 1, SUSPENDED); // 10 3

	sch_task_create (motor_toggle_speed_control, 69, SUSPENDED); // 11 np
	sch_task_create (motor_toggle_on_off, 69, SUSPENDED); // 12 np

	while (1)
	{
		sch_task_runner ();
	}
}


ISR (TIMER0_OVF_vect)
{
	TCNT0 = 0x83;
	sch_task_time_manager ();
}
