#include "blink.h"
#include "iomacros.h"
#include "motor.h"
#include "sch.h"
#include "tasks.h"

// Prototypes:
static void
blink_handler (void);

static uint16_t
speed2period (uint16_t speed);


void
blink (void)
{
	if (motor_is_on)
		blink_handler ();
	else
	{
		sch_set_task_state (TASK_BLINK_UPPER, SUSPENDED);
		sch_set_task_state (TASK_BLINK_LOWER, SUSPENDED);
	}
}


static void
blink_handler (void)
{
	sch_set_task_period (TASK_BLINK_UPPER, speed2period (OCR1A));
	sch_set_task_period (TASK_BLINK_LOWER, speed2period (OCR1A));

	switch (motor_state)
	{
	case MOTOR_RAMPUP_STATE:
	case MOTOR_HOLD_BEFORE_RAMPUP_STATE:
		sch_set_task_state (TASK_BLINK_UPPER, PENDING);
		sch_set_task_state (TASK_BLINK_LOWER, SUSPENDED);
		break;
	default:
		sch_set_task_state (TASK_BLINK_LOWER, PENDING);
		sch_set_task_state (TASK_BLINK_UPPER, SUSPENDED);
		break;
	}
}

void
blink_upper (void)
{
	LED_UP_ON;
	sch_set_task_state (TASK_BLINK_UPPER_SECONDARY, PENDING);
}

void
blink_lower (void)
{
	LED_DOWN_ON;
	sch_set_task_state (TASK_BLINK_LOWER_SECONDARY, PENDING);
}

void
blink_upper_secondary (void)
{
	LED_UP_OFF;
	sch_set_task_state (TASK_BLINK_UPPER_SECONDARY, SUSPENDED);
}

void
blink_lower_secondary (void)
{
	LED_DOWN_OFF;
	sch_set_task_state (TASK_BLINK_LOWER_SECONDARY, SUSPENDED);
}


static uint16_t
speed2period (uint16_t speed)
{
	return (((MOTOR_MAX_SPEED - speed) * 3) + 55) / 4;
}
