/**
 * @file    sch.c
 * @author  Ferenc Nemeth
 * @date    21 Jul 2018
 * @brief   This is a really simple, non-preemptive task scheduler.
 *          You can register tasks with their runnable function and the periodic
 * time you want to call them. With a help of a timer the tasks get into READY
 * state after every time period (except if they are SUSPENDED) and they get
 * called and executed in the main()'s inifinte loop. After they are finished
 * everything starts over. This Scheduler helps you to keep your tasks and
 * timing organized.
 *
 *          Copyright (c) 2018 Ferenc Nemeth - https://github.com/ferenc-nemeth/
 */

#include "sch.h"

// Static function prototypes:
static void
task_check_counter_and_handle_state (uint8_t task_index);


static task task_array[SCH_MAX_TASK_NUM]; /**< Variables and information for
                                                 every single task. */
static uint8_t task_number = 0; /**< Number of the registered tasks. */


/**
 * @brief   This function registers the tasks.
 *          At the beginning there is an error check for registration.
 *          If everything is good, the input values are saved.
 * @param   function: The task we want to call periodically.
 * @param   run_period: The time it gets called periodically.
 * @param   state: The state it starts (recommended state: PENDING).
 * @return  return_enum: Feedback about the success or cause of error of the
 * registration.
 */
return_enum
sch_task_create (task_function_ptr function,
                 task_counter_t run_period,
                 task_state state)
{
	return_enum exit = ERR_UNKNOWN;

	/* Null pointer. */
	if (task_array == NULL)
	{
		exit = ERR_NULL_PTR;
	}

	/* Time limit. */
	else if ((run_period < TASK_MIN_PERIOD) || (run_period > TASK_MAX_PERIOD))
	{
		exit = ERR_TIME_LIMIT;
	}

	/* Task number limit. */
	else if (task_number >= SCH_MAX_TASK_NUM)
	{
		exit = ERR_COUNT_LIMIT;
	}

	/* Everything is fine, save. */
	else
	{
		task_array[task_number].run = function;
		task_array[task_number].run_period = run_period;
		task_array[task_number].state = state;
		task_array[task_number].counter = 1;
		task_number++;
		exit = OK;
	}

	return exit;
}



/**
 * @brief   This function keeps track of the tasks' time and puts them into
 * 			READY state. This function SHALL be called in a timer interrupt.
 * @param   void
 * @return  void
 */
void
sch_task_time_manager (void)
{
	for (uint8_t task_index = 0; task_index < task_number; task_index++)
		if (task_array[task_index].state != SUSPENDED)
			task_check_counter_and_handle_state (task_index);
}

/**
 * @brief This is a backend method used in
 *		  the function "sch_task_time_manager".
 */
static void
task_check_counter_and_handle_state (uint8_t task_index)
{
	/* Put it into READY state. */
	if (task_array[task_index].counter >= task_array[task_index].run_period)
	{
		task_array[task_index].counter = 1;
		task_array[task_index].state = READY;
	}

	/* Or increment task's counter. */
	else
	{
		task_array[task_index].counter++;
	}
}



/**
 * @brief   This function calls the READY tasks and then puts them back into
 * PENDING state. This function SHALL be called in the infinite loop.
 * @param   void
 * @return  void
 */
void
sch_task_runner (void)
{
	for (uint8_t task_index = 0; task_index < task_number; task_index++)
	{
		/* If it is ready, call it.*/
		if (task_array[task_index].state == READY)
		{
			task_array[task_index].state = PENDING;
			task_array[task_index].run ();
		}
	}
}



/**
 * @brief   Returns the state of the task.
 * @param   task_number: Which task's state.
 * @return  task_state: state of the task.
 */
task_state
sch_get_task_state (uint8_t task_index)
{
	return task_array[task_index].state;
}



/**
 * @brief   Returns the burst time of the task.
 * @param   task_number: Which task's burst time.
 * @return  The burst time.
 */
task_counter_t
sch_get_task_period (uint8_t task_index)
{
	return task_array[task_index].run_period;
}



/**
 * @brief   Returns the current counter value of the task.
 * @param   task_number: Which task's counter.
 * @return  The counter.
 */
task_counter_t
sch_get_task_counter (uint8_t task_index)
{
	return task_array[task_index].counter;
}



/**
 * @brief   Manually changes the task's state.
 * @param   task_number: Which task's new state.
 * @param   new_state: The new state of the task.
 * @return  void
 */
void
sch_set_task_state (uint8_t task_index, task_state new_state)
{
	task_array[task_index].state = new_state;
	if (new_state == SUSPENDED)
		task_array[task_index].counter = 1;
}



/**
 * @brief   Manually changes the task's burst time.
 * @param   task_number: Which task's new burst time.
 * @param   new_period: The new burst time of the task.
 * @return  void
 */
void
sch_set_task_period (uint8_t task_index, task_counter_t new_period)
{
	task_array[task_index].run_period = new_period;
}



/**
 * @brief   Manually changes the task's counter.
 * @param   task_number: Which task's new counter value.
 * @param   new_counter: The new counter value of the task.
 * @return  void
 */
void
sch_set_task_counter (uint8_t task_index, task_counter_t new_counter)
{
	task_array[task_index].counter = new_counter;
}
