#ifndef BLINKER_H_
#define BLINKER_H_

void
blink (void);

void
blink_upper (void);

void
blink_lower (void);

void
blink_upper_secondary (void);

void
blink_lower_secondary (void);

#endif /* BLINKER_H_ */
