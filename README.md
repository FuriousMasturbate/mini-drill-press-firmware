# mini-drill-press project notes

## **Initialization**

Please run the command below in project's root:

```bash
make init
```

Or:

```bash
git config core.hooksPath .githooks
```

In order to add the 'pre-commit' hook to git.

---

## **TO-DO**

Nothing o_O

---

## **I/O**

### **inputs**

- Button : PD3

### **outputs**

- LED : PD2,PD4
- Motor : OC1A = PB1

---
