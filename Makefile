# simple AVR Makefile
#
# written by michael cousins (http://github.com/mcous)
# released to the public domain

# Makefile

# parameters (change this stuff accordingly)
# project name
PRJ = mdp
# avr mcu
MCU = atmega8a
PRGMCU = m8
# mcu clock frequency
CLK = 8000000UL
# avr programmer (and port if necessary)
# e.g. PRG = usbtiny -or- PRG = arduino -P /dev/tty.usbmodem411
PRG = usbasp-clone
# fuse values for avr: low, high, and extended
# these values are from an Arduino Uno (ATMega328P)
# see http://www.engbedded.com/fusecalc/ for other MCUs and options
LFU = 0x04
HFU = 0xD9
EFU =

# some paths

# where to look for source files and external libraries
#(consisting of .c/.cpp files and .h files)
# e.g. EXT = ../../EyeToSee ../../YouSART
SRCS = src src/tasks
# header files (include path)
INCLUDE = include -I include/tasks
# path to scheduler tasks
#TASKS = src/tasks
# path for .hex and .elf files
REL = release

# compile options

# compiler warning flags
WFLAGS = -std=c99 -pedantic -Wall \
    -Wno-missing-braces -Wextra -Wno-missing-field-initializers -Wformat=2 \
    -Wswitch-default -Wswitch-enum -Wcast-align -Wpointer-arith \
    -Wbad-function-cast -Wstrict-overflow=5 -Wstrict-prototypes -Winline \
    -Wnested-externs -Wcast-qual -Wshadow -Wunreachable-code \
    -Wlogical-op -Wfloat-equal -Wstrict-aliasing=2 -Wredundant-decls \
    -Wold-style-definition -Wdouble-promotion -Werror \

# other compile options
EXTRAFLAGS = -funsigned-char -funsigned-bitfields -fshort-enums \
 -fpack-struct -fno-jump-tables

###############################################################
#                     G O O D S T U F F                       #
###############################################################

# include path

# c flags
CFLAGS = -std=c99 $(WFLAGS) $(EXTRAFLAGS) -Werror -Os $(DEBUG) -DF_CPU=$(CLK) -mmcu=$(MCU) -I $(INCLUDE)
CPPFLAGS =

# executables
AVRDUDE = avrdude -c $(PRG) -p $(PRGMCU)
OBJCOPY = avr-objcopy
OBJDUMP = avr-objdump
SIZE    = avr-size --format=avr --mcu=$(MCU)
CC      = avr-gcc

# generate list of objects
SRCX     := $(foreach dir, $(SRCS), $(wildcard $(dir)/*.c))
SRCPPX   := $(foreach dir, $(SRCPPS), $(wildcard $(dir)/*.cpp))
OBJ       = $(SRCX:.c=.o) $(SRCPPX:.cpp=.o)
HEX = $(REL)/$(PRJ).hex
ELF = $(REL)/$(PRJ).elf

# targets

.PHONY: test fuse clear clean size init help

# clean and build for release
all: clean build clear

# clean and build for debug
debug: clean set-debug build clear

# set debug flags for analysis of elf file
set-debug:
	$(eval DEBUG := -g -fdebug-prefix-map=/=)

# compile all files
build: $(HEX)

# elf file
$(ELF): $(OBJ)
	$(CC) $(CFLAGS) -o $(ELF) $(OBJ)

# hex file
$(HEX): $(ELF)
	rm -f $(HEX)
	$(OBJCOPY) -j .text -j .data -O ihex $(ELF) $(HEX)
	$(SIZE) $(ELF)

# other targets
# objects from c files
.c.o:
	@$(CC) $(CFLAGS) -c $< -o $@

# objects from c++ files
.cpp.o:
	@$(CC) $(CFLAGS) $(CPPFLAGS) -c $< -o $@

## other targets

# test programmer connectivity
test:
	$(AVRDUDE) -v

# flash program to mcu
flash: all
	$(AVRDUDE) -U flash:w:$(HEX):i

# write fuses to mcu
fuse:
	$(AVRDUDE) -U lfuse:w:$(LFU):m -U hfuse:w:$(HFU):m
	#-U efuse:w:$(EFU):m #FOR EXTENDED FUSE#

# generate disassembly files for debugging
disasm: $(ELF)
	$(OBJDUMP) -d $(ELF)

# show size of functions in a sorted manner
size:
	@echo
	@avr-nm -Crtd --size-sort $(ELF)
	@echo

# remove compiled files
clean:
	@rm -f $(HEX) $(ELF) *.o
	@$(foreach dir, $(SRCS), rm -f $(dir)/*.o;)

# remove after-build unnecessary files
clear:
	@rm -f *.o
	@$(foreach dir, $(SRCS), rm -f $(dir)/*.o;)

init:
	git config core.hooksPath .githooks

# help lmao
help:
	@echo
	@echo "~AVR-Makefile~"
	@echo
	@echo "~~~"
	@echo "Commands:"
	@echo "  make         compile and build all for release"
	@echo "  debug        compile and build all for debug"
	@echo "  test         test programmer connectivity"
	@echo "  flash        build and flash to mcu"
	@echo "  fuse         write fuses to mcu"
	@echo "  disasm       get disassembly for debug"
	@echo "  size         show size of functions in a sorted manner"
	@echo "  clean        build again"
	@echo "~~~"
	@echo
